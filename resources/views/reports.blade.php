<?php session_start(); ?>
        <!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Dashboard | </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">



    <section id="body-container" style="margin-left:155px; margin-right:155px;" class="animsition dashboard-page">
    <div class="conter-wrapper">
        <div class="row">
            <div class="col-md-12 "`>

                <img src="{{url('img/logo.png')}}" style="width:100px; margin-left:45%"> <br>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" align="center"> PMA Report</h3>
                    </div>
                    <p style="font-size:16px; text-align: center">There are {{count($doctors)}} doctors and {{count($patients)}} patients on the system.
                        {{count($healthtips)}} articles have been posted and {{count($appointments)}} appointments made in total.
                    </p>

                    <div class="panel-body">
                    <h3>DOCTORS</h3>
                        <table class="table ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Region</th>
                                <th>Added On</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($doctors as $item)
                                <tr>
                                    <td> {{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>Weija</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{$item->region}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <h3>PATIENTS</h3>

                        <table class="table ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Region</th>
                                <th>Added On</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($patients as $item)
                                <tr>
                                    <td> {{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>Weija</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{$item->region}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>


</section>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>

</body>

</html>